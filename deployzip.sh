#!/bin/bash
# Call this script with the name of the zip file 

#echo "Listing zip file $1 contents into temp.txt"
unzip -ql $1  | awk '{if ($1 > 0 && $1 != "Length" && $4 != "") print $4}' > temp.txt
for i in `cat temp.txt`; do cp -a $i $i.`date +%y%m%d`;echo $?;done
unzip -o $1
rm -f $1

#echo "Backing up files in temp.txt with yymmdd timestamp, 0 for file copied successfully"