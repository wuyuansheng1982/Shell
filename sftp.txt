usermod -a -G group user
gpasswd -d user group

ChrootDirectory
Specifies the pathname of a directory to chroot(2) to after authentication. All components of the pathname must be root-owned directories that are not writable by any other user or group. After the chroot, sshd(8) changes the working directory to the user's home directory.

For uploads, create incoming, cltmsgs, temp update directory with write access, owner as user.
For downloads, create batch, data, 


	'S' = The directory's setgid bit is set, but the execute bit isn't set.
	's' = The directory's setgid bit is set, and the execute bit is set.

	SetGID = When another user creates a file or directory under such a setgid directory, the new file or directory will have its group set as the group of the directory's owner, instead of the group of the user who creates it.
