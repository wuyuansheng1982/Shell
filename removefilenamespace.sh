 #!/usr/bin/ksh

 typeset oldfile=""
 typeset newfile=""

 # the grep filters for filenames with spaces only:
 ls -1 | grep "[^ ] [^ ]" | while read oldfile ; do
      newfile="$( print - $oldfile | tr -d ' ')"
      print - "mv \"${oldfile}\" \"${newfile}\"" # just display, next line is working
      # mv "$oldfile" "$newfile"
 done
 ~
