# #################################################
# DBA BUNDLE 2.0
# Extract the DBA_BUNDLE & Set Command Aliases
# 					#   #     #
# Author:	Mahmmoud ADEL	      # # # #   ###
# Created:	02-01-14	    #   #   # #   #  
# Modified:	13-01-14
#		Force using "." command 
#
# #################################################
SRV_NAME=`uname -n`

USEDCOMM=`history|tail -1|awk '{print $2}'|egrep '(bundle|source|\.)'|grep -v '\.\/'`
        if [ -z "${USEDCOMM}" ]
         then
          echo ""
          echo "Please Use \".\" command to run this script."
          echo "e.g."
          echo ". ~/DBA_BUNDLE2/aliases_DBA_BUNDLE.sh"
          echo ""
	  #exit 1
	  return
        fi

# ###########################
# Extract The Bundle:
# ###########################

# Check the existance of the TAR file:
#	if [ -f ./DBA_BUNDLE2.tar ]
#	 then
#	  echo "Extracting The DBA_BUNDLE..."
#	  tar xvf ./DBA_BUNDLE.tar
#	 else
#	  echo "The TAR file DBA_BUNDLE.tar is not exist under the current working directory !"
#	  echo "Please copy the TAR file DBA_BUNDLE.tar to the current working directory and re-run the script."
#	  exit
#	fi

# ###########################
# Listing Available Databases:
# ###########################

# Count Instance Numbers:
INS_COUNT=$( ps -ef|grep pmon|grep -v grep|grep -v ASM|wc -l )

# Exit if No DBs are running:
if [ $INS_COUNT -eq 0 ]
 then
   echo No Database is Running !
   echo
   return
fi

# If there is ONLY one DB set it as default without prompt for selection:
if [ $INS_COUNT -eq 1 ]
 then
   export ORACLE_SID=$( ps -ef|grep pmon|grep -v grep|grep -v ASM|awk '{print $NF}'|sed -e 's/ora_pmon_//g'|grep -v sed|grep -v "s///g" )

# If there is more than one DB ASK the user to select:
elif [ $INS_COUNT -gt 1 ]
 then
    echo
    echo "Select the ORACLE_SID:[Enter the number]"
    echo ---------------------
    select DB_ID in $( ps -ef|grep pmon|grep -v grep|grep -v ASM|awk '{print $NF}'|sed -e 's/ora_pmon_//g'|grep -v sed|grep -v "s///g" )
     do
        if [ -z "${REPLY##[0-9]*}" ]
         then
          export ORACLE_SID=$DB_ID
	  echo
          echo Selected Instance:
	  echo "********"
          echo $DB_ID
          echo "********"
          break
         else
          export ORACLE_SID=${REPLY}
          break
        fi
     done

fi
# Exit if the user selected a Non Listed Number:
        if [ -z "${ORACLE_SID}" ]
         then
          echo "You've Entered An INVALID ORACLE_SID"
          exit
        fi

# #########################
# Getting ORACLE_HOME
# #########################
  ORA_USER=`ps -ef|grep ${ORACLE_SID}|grep pmon|grep -v grep|grep -v ASM|awk '{print $1}'|tail -1`
  USR_ORA_HOME=`grep ${ORA_USER} /etc/passwd| cut -f6 -d ':'|tail -1`

## If OS is Linux:
if [ -f /etc/oratab ]
  then
  ORATAB=/etc/oratab
  ORACLE_HOME=`grep -v '^\#' $ORATAB | grep -v '^$'| grep -i "^${ORACLE_SID}:" | perl -lpe'$_ = reverse' | cut -f3 | perl -lpe'$_ = reverse' |cut -f2 -d':'`
  export ORACLE_HOME

## If OS is Solaris:
elif [ -f /var/opt/oracle/oratab ]
  then
  ORATAB=/var/opt/oracle/oratab
  ORACLE_HOME=`grep -v '^\#' $ORATAB | grep -v '^$'| grep -i "^${ORACLE_SID}:" | perl -lpe'$_ = reverse' | cut -f3 | perl -lpe'$_ = reverse' |cut -f2 -d':'`
  export ORACLE_HOME
fi

## If oratab is not exist, or ORACLE_SID not added to oratab, find ORACLE_HOME in user's profile:
if [ -z "${ORACLE_HOME}" ]
 then
  ORACLE_HOME=`grep -h 'ORACLE_HOME=\/' $USR_ORA_HOME/.bash* $USR_ORA_HOME/.*profile | perl -lpe'$_ = reverse' |cut -f1 -d'=' | perl -lpe'$_ = reverse'|tail -1`
  export ORACLE_HOME
fi

# #########################
# Getting DB_NAME:
# #########################
VAL1=$(${ORACLE_HOME}/bin/sqlplus -S "/ as sysdba" <<EOF
set pages 0 feedback off;
prompt
SELECT name from v\$database
exit;
EOF
)
# Getting DB_NAME in Uppercase & Lowercase:
DB_NAME_UPPER=`echo $VAL1| perl -lpe'$_ = reverse' |awk '{print $1}'|perl -lpe'$_ = reverse'`
DB_NAME_LOWER=$( echo "$DB_NAME_UPPER" | tr -s  '[:upper:]' '[:lower:]' )
export DB_NAME_UPPER
export DB_NAME_LOWER

# DB_NAME is Uppercase or Lowercase?:

     if [ -f $ORACLE_HOME/diagnostics/${DB_NAME_UPPER} ]
        then
                DB_NAME=$DB_NAME_UPPER
        else
                DB_NAME=$DB_NAME_LOWER
     fi

# #########################
# Getting ALERTLOG path:
# #########################
DUMP=$(${ORACLE_HOME}/bin/sqlplus -S "/ as sysdba" <<EOF
set pages 0 feedback off;
prompt
SELECT value from v\$parameter where NAME='background_dump_dest';
exit;
EOF
)
ALERTZ=`echo $DUMP | perl -lpe'$_ = reverse' |awk '{print $1}'|perl -lpe'$_ = reverse'`
ALERTDB=${ALERTZ}/alert_${ORACLE_SID}.log

# ########################
# Getting ORACLE_BASE:
# ########################

# Get ORACLE_BASE from user's profile if it EMPTY:

if [ -z "${ORACLE_BASE}" ]
 then
  ORACLE_BASE=`grep -h 'ORACLE_BASE=\/' $USR_ORA_HOME/.bash* $USR_ORA_HOME/.*profile | perl -lpe'$_ = reverse' |cut -f1 -d'=' | perl -lpe'$_ = reverse'|tail -1`
  export ORACLE_BASE
fi

# Setting the BUNDLE Environment:

PROFILE=${USR_ORA_HOME}/.DBA_BUNDLE_profile

        if [ -f ${USR_ORA_HOME}/.bashrc ]
         then
	  USRPROF=${USR_ORA_HOME}/.bashrc
	  sed '/DBA_BUNDLE/d' ${USRPROF} > ${USRPROF}.tmp && mv ${USRPROF}.tmp ${USRPROF}
          echo "# The Following Entry For DBA_BUNDLE Aliases" >> ${USRPROF}
	  echo ". ${PROFILE}" >> ${USRPROF}
	fi
	if [ -f ${USR_ORA_HOME}/.profile ]
	 then
	  USRPROF=${USR_ORA_HOME}/.profile
          sed '/DBA_BUNDLE/d' ${USRPROF} > ${USRPROF}.tmp && mv ${USRPROF}.tmp ${USRPROF}
          echo "# The Following Entry For DBA_BUNDLE Aliases" >> ${USRPROF}
          echo ". ${PROFILE}" >> ${USRPROF}
	fi
        if [ -f ${USR_ORA_HOME}/.bash_profile ]
	 then
          USRPROF=${USR_ORA_HOME}/.bash_profile
          sed '/DBA_BUNDLE/d' ${USRPROF} > ${USRPROF}.tmp && mv ${USRPROF}.tmp ${USRPROF}
          echo "# The Following Entry For DBA_BUNDLE Aliases" >> ${USRPROF}
          echo ". ${PROFILE}" >> ${USRPROF}
        fi

# ##########################################
# Setting the Environment & Commands Alieses
# ##########################################
if [ -f ${USR_ORA_HOME}/DBA_BUNDLE2/aliases_DBA_BUNDLE.sh ]
then
echo ""
echo "Setting Up Alieses..."

PATH=$PATH:$ORACLE_HOME/bin
export PATH
TNS_ADMIN=${ORACLE_HOME}/network/admin
export TNS_ADMIN
#sed -i '/DBA_BUNDLE2/d' ${PROFILE}
        if [ -f ${PROFILE} ]; then
	 sed '/DBA_BUNDLE/d' ${PROFILE} > ${PROFILE}.tmp && mv ${PROFILE}.tmp ${PROFILE}
	fi
echo "# DBA_BUNDLE2  ====================================================================================="  >> ${PROFILE}
echo "# DBA_BUNDLE2  The Following ALIASES Are Added By aliases_DBA_BUNDLE.sh Script [Part of DBA_BUNDLE2]"  >> ${PROFILE}
echo "# DBA_BUNDLE2  ====================================================================================="  >> ${PROFILE}
echo "ORACLE_SID=${ORACLE_SID}       #DBA_BUNDLE2" >> ${PROFILE}
echo "export ORACLE_SID        #DBA_BUNDLE2" >> ${PROFILE}
echo "alias l='ls'             #DBA_BUNDLE2" >> ${PROFILE}
echo "alias d='date'           #DBA_BUNDLE2 >> Display the date." >> ${PROFILE}
echo "alias df='df -h'         #DBA_BUNDLE2" >> ${PROFILE}
echo "alias top='top -c'       #DBA_BUNDLE2" >> ${PROFILE}
echo "alias ll='ls -rtlh'      #DBA_BUNDLE2" >> ${PROFILE}
echo "alias lla='ls -rtlha'    #DBA_BUNDLE2" >> ${PROFILE}
echo "alias cron='crontab -e'  #DBA_BUNDLE2 >> Open the crontab for editing." >> ${PROFILE}
echo "alias crol='crontab -l'  #DBA_BUNDLE2 >> Display the crontab." >> ${PROFILE}
echo "alias profile='. ${PROFILE}'        #DBA_BUNDLE2 >> Call the user's profile to reload Environment Variables." >> ${PROFILE}
echo "alias viprofile='vi ${PROFILE}'     #DBA_BUNDLE2 >> Open the user's profile for editing." >> ${PROFILE}
echo "alias catprofile='cat ${PROFILE}'   #DBA_BUNDLE2 >> Display the user's profile." >> ${PROFILE}
echo "alias alert='tail -100f ${ALERTDB}' #DBA_BUNDLE2 >> Tail DB ALERTLOG" >> ${PROFILE}
echo "alias vialert='vi ${ALERTDB}'       #DBA_BUNDLE2 >> vi DB ALERTLOG" >> ${PROFILE}
echo "alias logs='cd ~/Logs;ls -rtlh'     #DBA_BUNDLE2" >> ${PROFILE}
echo "alias sql='sqlplus "/ as sysdba"'   #DBA_BUNDLE2" >> ${PROFILE}
echo "alias grid='cd $GRID_HOME; ls; pwd' #DBA_BUNDLE2 >> Step you in the GRID_HOME if installed." >> ${PROFILE}
echo "alias oh='cd ${ORACLE_HOME};ls;pwd'              #DBA_BUNDLE2 >> Step you under ORACLE_HOME." >> ${PROFILE}
echo "alias bundle=' cd ${USR_ORA_HOME}/DBA_BUNDLE2;. aliases_DBA_BUNDLE.sh;ls;pwd' #DBA_BUNDLE2 >> Step you under ORACLE_HOME." >> ${PROFILE}
echo "alias p='ps -ef|grep pmon|grep -v grep'          #DBA_BUNDLE2 >> List current RUNNING Instances." >> ${PROFILE}
echo "alias lsn='ps -ef|grep lsn|grep -v grep'         #DBA_BUNDLE2 >> List current RUNNING Listeners." >> ${PROFILE}
echo "alias bdump='cd ${ALERTZ};ls -lrt|tail -10;pwd'  #DBA_BUNDLE2 >> Step under bdump" >> ${PROFILE}
echo "alias dbs='cd ${ORACLE_HOME}/dbs;ls -rtlh;pwd'   #DBA_BUNDLE2 >> Step you under ORACLE_HOME/dbs directory." >> ${PROFILE}
echo "alias rman='${ORACLE_HOME}/bin/rman target /'    #DBA_BUNDLE2" >> ${PROFILE}
echo "alias lis='vi ${ORACLE_HOME}/network/admin/listener.ora'             #DBA_BUNDLE2" >> ${PROFILE}
echo "alias tns='vi ${ORACLE_HOME}/network/admin/tnsnames.ora'             #DBA_BUNDLE2" >> ${PROFILE}
echo "alias pfile='vi ${ORACLE_HOME}/dbs/init${ORACLE_SID}.ora'            #DBA_BUNDLE2" >> ${PROFILE}
echo "alias aud='cd ${ORACLE_HOME}/rdbms/audit;ls -rtl|tail -200'          #DBA_BUNDLE2" >> ${PROFILE}
echo "alias network='cd ${ORACLE_HOME}/network/admin;ls -rtlh;pwd'         #DBA_BUNDLE2" >> ${PROFILE}
echo "alias spfile='vi ${ORACLE_HOME}/dbs/spfile${ORACLE_SID}.ora'         #DBA_BUNDLE2" >> ${PROFILE}
echo "alias raclog='tail -100f $GRID_HOME/log/${SRV_NAME}/alert${SRV_NAME}.log' #DBA_BUNDLE2 >> Monitor RAC ALERTLOG on the fly." >> ${PROFILE}
echo "alias removebundle='sh ${USR_ORA_HOME}/DBA_BUNDLE2/bundle_remove.sh' #DBA_BUNDLE2 >> Remove All Modifications done by aliases script." >> ${PROFILE}
echo "alias dfs='sh ${USR_ORA_HOME}/DBA_BUNDLE2/datafiles.sh'              #DBA_BUNDLE2 >> List All DATAFILES on the database." >> ${PROFILE}
echo "alias datafiles='sh ${USR_ORA_HOME}/DBA_BUNDLE2/datafiles.sh'        #DBA_BUNDLE2 >> List All DATAFILES on the database." >> ${PROFILE}
echo "alias invalid='sh ${USR_ORA_HOME}/DBA_BUNDLE2/invalid_objects.sh'    #DBA_BUNDLE2 >> List All Invalid Objects." >> ${PROFILE}
echo "alias objects='sh ${USR_ORA_HOME}/DBA_BUNDLE2/biggest_50_objects.sh' #DBA_BUNDLE2 >> List Biggest 50 Object on the database." >> ${PROFILE}
echo "alias unlock='sh ${USR_ORA_HOME}/DBA_BUNDLE2/unlock_user.sh'         #DBA_BUNDLE2 >> Unlock a specific DB User Account." >> ${PROFILE}
echo "alias sessions='sh ${USR_ORA_HOME}/DBA_BUNDLE2/all_sessions_info.sh' #DBA_BUNDLE2 >> List All current sessions on the DB." >> ${PROFILE}
echo "alias session='sh ${USR_ORA_HOME}/DBA_BUNDLE2/session_details.sh'  #DBA_BUNDLE2 >> List Details of a current session." >> ${PROFILE}
echo "alias locks='sh ${USR_ORA_HOME}/DBA_BUNDLE2/db_locks.sh'           #DBA_BUNDLE2 >> Show Blocking LOCKS on the database" >> ${PROFILE}
echo "alias sqlid='sh ${USR_ORA_HOME}/DBA_BUNDLE2/sql_id_details.sh'     #DBA_BUNDLE2 >> Show a specific SQL Statmnt details." >> ${PROFILE}
echo "alias parm='sh ${USR_ORA_HOME}/DBA_BUNDLE2/parameter_val.sh'       #DBA_BUNDLE2 >> Show the value of a Visible/Hidden DB Parameter." >> ${PROFILE}
echo "alias jobs='sh ${USR_ORA_HOME}/DBA_BUNDLE2/db_jobs.sh'             #DBA_BUNDLE2 >> List All database Jobs." >> ${PROFILE}
echo "alias spid='sh ${USR_ORA_HOME}/DBA_BUNDLE2/process_info.sh'        #DBA_BUNDLE2 >> Show Session details providing it's Unix PID." >> ${PROFILE}
echo "alias tbs='sh ${USR_ORA_HOME}/DBA_BUNDLE2/tablespaces.sh'          #DBA_BUNDLE2 >> List All TABLESPACES on the database." >> ${PROFILE}
echo "alias tablespaces='sh ${USR_ORA_HOME}/DBA_BUNDLE2/tablespaces.sh'  #DBA_BUNDLE2 >> List All TABLESPACES on the database." >> ${PROFILE}
echo "alias cleanup='sh ${USR_ORA_HOME}/DBA_BUNDLE2/oracle_cleanup.sh'   #DBA_BUNDLE2 >> Backup & Clean up All DB & it's Listener LOGs." >> ${PROFILE}
echo "alias starttrace='sh ${USR_ORA_HOME}/DBA_BUNDLE2/start_tracing.sh' #DBA_BUNDLE2 >> Start TRACING an Oracle Session." >> ${PROFILE}
echo "alias stoptrace='sh ${USR_ORA_HOME}/DBA_BUNDLE2/stop_tracing.sh'   #DBA_BUNDLE2 >> Stop TRACING a traced Oracle Session." >> ${PROFILE}
echo "alias objectddl='sh ${USR_ORA_HOME}/DBA_BUNDLE2/object_ddl.sh'     #DBA_BUNDLE2 >> Generate the Creation DDL Statement for an OBJECT." >> ${PROFILE}
echo "alias userddl='sh ${USR_ORA_HOME}/DBA_BUNDLE2/user_ddl.sh'         #DBA_BUNDLE2 >> Generate Full SQL Creation script for DB USER." >> ${PROFILE}
echo "alias userdetail='sh ${USR_ORA_HOME}/DBA_BUNDLE2/user_details.sh'  #DBA_BUNDLE2 >> Generate USER DDL plus Schema information." >> ${PROFILE}
echo "alias schemadetail='sh ${USR_ORA_HOME}/DBA_BUNDLE2/user_details.sh' #DBA_BUNDLE2 >> Generate USER DDL plus Schema information." >> ${PROFILE}
echo "alias roleddl='sh ${USR_ORA_HOME}/DBA_BUNDLE2/role_ddl.sh'         #DBA_BUNDLE2 >> Generate Full SQL Creation script for DB ROLE." >> ${PROFILE}
echo "alias roledetail='sh ${USR_ORA_HOME}/DBA_BUNDLE2/role_ddl.sh'      #DBA_BUNDLE2 >> Generate Full SQL Creation script for DB ROLE." >> ${PROFILE}
echo "alias lastlogin='sh ${USR_ORA_HOME}/DBA_BUNDLE2/last_logon_report.sh' #DBA_BUNDLE2 >> Reports the last login date for ALL users on DB." >> ${PROFILE}
echo "alias failedlogin='sh ${USR_ORA_HOME}/DBA_BUNDLE2/failed_logins.sh'   #DBA_BUNDLE2 >> Report last failed login attempts on the DB." >> ${PROFILE}
echo "alias archivedel='sh ${USR_ORA_HOME}/DBA_BUNDLE2/Archives_Delete.sh'  #DBA_BUNDLE2 >> Delete Archivelogs older than n number of days." >> ${PROFILE}
echo "alias analyze='sh ${USR_ORA_HOME}/DBA_BUNDLE2/analyze_tables.sh' #DBA_BUNDLE2 >> Analyze All Tables in a Schema." >> ${PROFILE}
echo "alias audit='sh ${USR_ORA_HOME}/DBA_BUNDLE2/zAngA_zAngA.sh'    #DBA_BUNDLE2 >> Retreive AUDIT data for a DB user on a SPECIFIC DATE." >> ${PROFILE}
echo "alias zanga='sh ${USR_ORA_HOME}/DBA_BUNDLE2/zAngA_zAngA.sh'    #DBA_BUNDLE2 >> Retreive AUDIT data for a DB user on a SPECIFIC DATE." >> ${PROFILE}
echo "alias gather='sh ${USR_ORA_HOME}/DBA_BUNDLE2/gather_stats.sh'  #DBA_BUNDLE2 >> Backup & Gather Statistics for a SPECIFIC SCHEMA|TABLE." >> ${PROFILE}
echo "alias exportdata='sh ${USR_ORA_HOME}/DBA_BUNDLE2/export_data.sh'  #DBA_BUNDLE2 >> Export Database | SCHEMA | Table data with EXP or EXPDP." >> ${PROFILE}
echo "alias rmanfull='sh ${USR_ORA_HOME}/DBA_BUNDLE2/RMAN_full.sh'   #DBA_BUNDLE2 >> Takes an RMAN FULL DATABASE BACKUP." >> ${PROFILE}
echo "alias tableinfo='sh ${USR_ORA_HOME}/DBA_BUNDLE2/table_info.sh' #DBA_BUNDLE2 >> Show ALL Important Information about specific TABLE." >> ${PROFILE}
echo "alias tablerebuild='sh ${USR_ORA_HOME}/DBA_BUNDLE2/rebuild_table.sh' #DBA_BUNDLE2 >> REBUILD a TABLE and its related INDEXES." >> ${PROFILE}

source ${PROFILE}

echo ""
echo "*******************"
echo "LIST OF ALL ALIASES:"
echo "*******************"
echo 
echo " ==============================================================="
echo "|Alias          |Usage                                          |"
echo "|===============|===============================================|"
echo "|alert          |Open the Database Alertlog with tail -f        |"
echo "|---------------|-----------------------------------------------|"
echo "|vialert        |Open the Database Alertlog with vi editor      |"
echo "|---------------|-----------------------------------------------|"
echo "|oh             |Go to ORACLE_HOME Dir                          |"
echo "|---------------|-----------------------------------------------|"
echo "|bundle         |Go to Bundle Dir and run aliases_DBA_BUNDLE.sh |"
echo "|---------------|-----------------------------------------------|"
echo "|removebundle   |Remove bundle Aliases from the System.         |"
echo "|---------------|-----------------------------------------------|"
echo "|p              |List Running Instances                         |"
echo "|---------------|-----------------------------------------------|"
echo "|lsn            |List Running Listeners                         |"
echo "|---------------|-----------------------------------------------|"
echo "|lis            |Open listener.ora file with vi editor          |"
echo "|---------------|-----------------------------------------------|"
echo "|tns            |Open tnsnames.ora file with vi editor          |"
echo "|---------------|-----------------------------------------------|"
echo "|pfile          |Open the Instance PFILE with vi editor         |"
echo "|---------------|-----------------------------------------------|"
echo "|spfile         |Open the Instance SPFILE with vi editor        |"
echo "|---------------|-----------------------------------------------|"
echo "|dbs            |Go to ORACLE_HOME/dbs                          |"
echo "|---------------|-----------------------------------------------|"
echo "|aud            |Go to ORACLE_HOME/rdbms/audit                  |"
echo "|---------------|-----------------------------------------------|"
echo "|bdump          |Go to BACKGROUND_DUMP_DEST                     |"
echo "|---------------|-----------------------------------------------|"
echo "|network        |Go to ORACLE_HOME/network/admin                |"
echo "|---------------|-----------------------------------------------|"
echo "|raclog         |Open the Clusterware Alertlog                  |"
echo "|---------------|-----------------------------------------------|"
echo "|dfs/datafiles  |List All DATAFILES on a database               |"
echo "|---------------|-----------------------------------------------|"
echo "|tbs/tablespaces|List All TABLESPACES on a database             |"
echo "|---------------|-----------------------------------------------|"
echo "|invalid        |List All Invalid Objects + Fix                 |"
echo "|---------------|-----------------------------------------------|"
echo "|objects        |List the Biggest 50 Objects in the database    |"
echo "|---------------|-----------------------------------------------|"
echo "|session        |List Details of a current oracle session       |"
echo "|---------------|-----------------------------------------------|"
echo "|sessions       |List All current sessions details on RAC DB    |"
echo "|---------------|-----------------------------------------------|"
echo "|spid           |Show Session details Based on it's Unix PID    |"
echo "|---------------|-----------------------------------------------|"
echo "|sqlid          |Show SQL Statement details Based on it's SQL_ID|"
echo "|---------------|-----------------------------------------------|"
echo "|locks          |Show Blocking LOCKS on the database            |"
echo "|---------------|-----------------------------------------------|"
echo "|unlock         |Unlock a specific DB User Account              |"
echo "|---------------|-----------------------------------------------|"
echo "|parm           |Show a Visible/Hidden DB Parameter Value       |"
echo "|---------------|-----------------------------------------------|"
echo "|cleanup        |Backup & Clean up All DB & it's Listener LOGs  |"
echo "|---------------|-----------------------------------------------|"
echo "|lastlogin      |Shows the last login date for ALL users on DB  |"
echo "|---------------|-----------------------------------------------|"
echo "|starttrace     |Start TRACING an Oracle Session                |"
echo "|---------------|-----------------------------------------------|"
echo "|stoptrace      |Stop TRACING a current Traced Oracle Session   |"
echo "|---------------|-----------------------------------------------|"
echo "|userddl        |Generate Full SQL Creation script for a DB User|"
echo "|---------------|-----------------------------------------------|"
echo "|schemadetail   |Generate USER DDL + Schema Information         |"
echo "|---------------|-----------------------------------------------|"
echo "|roleddl        |Generate Full SQL Creation script for a DB ROLE|"
echo "|---------------|-----------------------------------------------|"
echo "|objectddl      |Generate Full SQL Creation script for an Object|"
echo "|---------------|-----------------------------------------------|"
echo "|failedlogin    |Report Failed Login Attempts in the last n days|"
echo "|---------------|-----------------------------------------------|"
echo "|archivedel     |Delete Archivelogs older than n number of days |"
echo "|---------------|-----------------------------------------------|"
echo "|analyze        |Analyze All tables in a specific SCHEMA        |"
echo "|---------------|-----------------------------------------------|"
echo "|audit/zanga    |Retreive AUDIT data for DB users               |"
echo "|---------------|-----------------------------------------------|"
echo "|gather         |Gather STATISTICS on a SCHEMA or TABLE         |"
echo "|---------------|-----------------------------------------------|"
echo "|rmanfull       |Take RMAN FULL BACKUP for the database         |"
echo "|---------------|-----------------------------------------------|"
echo "|exportdata     |Export DB|SCHEMA|TABLE data using EXP or EXPDP |"
echo "|---------------|-----------------------------------------------|"
echo "|tableinfo      |Show Important Information for a specific TABLE|"
echo "|---------------|-----------------------------------------------|"
echo "|tablerebuild   |REBUILD A TABLE and its related INDEXES        |"
echo " ==============================================================="	
echo ""
echo "The Following Scripts Doesn't have Aliases:"
echo "******************************************"
echo " --------------------------------------------------------------- "
echo "|dbalarm.sh     |Schedule this script in the crontab to run     |"
echo "|               |[every 5 minutes] to Monitor CPU, Filesystem   |"
echo "|               |utilization and report ORA & TNS errors that   |"
echo "|               |appear in the ALERTLOG for Databases& Listeners|"
echo "|               |that run on the server to your E-MAIL address: |"
echo "|               |In line# 19 modify this template:              |"
echo "|               |  <youremail@yourcompany.com>                  |"
echo "|               |  to your E-mail Address.                      |"
echo "|---------------|-----------------------------------------------|"
echo "|dbdailychk.sh  |Schedule this script in the crontab to run     |"
echo "|               |[ONE TIME A DAY] to perform some daily health  |"
echo "|               |checks on the database like:                   |"
echo "|               |CHECKING ALL DBs/Listeners ALERTLOGS FOR ERRORS|"
echo "|               |CHECKING Filesystem/CPU/Tablespaces utilization|"
echo "|               |CHECKING UNUSABLE INDEXES/INVALID OBJECTS/AUDIT|"
echo "|               |RECORDS/CORRUPTED BLOCKS/FAILED LOGINS/FAILED  |"
echo "|               |JOB.                                           |"
echo "|               |Point this line in the script to your E-MAIL:  |"
echo "|               |MAIL_LIST=youremail@yourcompany.com            |"
echo "|---------------|-----------------------------------------------|"
echo "|SHUTDOWN_All.sh|SHUTDOWN ALL Databases and Listeners           |"
echo "|               |running on The server, I didn't alias it, to   |"
echo "|               |avoid running it by mistake!.                  |"
echo "|---------------|-----------------------------------------------|"
echo "|COLD_BACKUP.sh |-Take a COLD BACKUP for any database.          |"
echo "|               |-Create a Restore Script that help you restore |"
echo "|               | the taken Cold Backup later.                  |"
echo " --------------------------------------------------------------- "
echo ""
echo "***************************************"
echo "Thanks for using DBA BUNDLE,"
echo "Mahmmoud ADEL"
echo "Oracle DBA"
echo "dba-tips.blogspot.com"
echo "***************************************"
echo ""

else
 echo "The Bundle directory ${USR_ORA_HOME}/DBA_BUNDLE2 is not exist!"
 echo "The DBA_BUNDLE Tar File MUST be extracted under Oracle Owner Home Directory: ${USR_ORA_HOME}"
 echo ""
fi

# #############
# END OF SCRIPT
# #############
# DISCLAIMER: THIS SCRIPT IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT WITHOUT ANY WARRANTY. IT IS PROVIDED "AS IS".
# DOWNLOAD LATEST VERSION OF DATABASE ADMINISTRATION BUNDLE FROM: http://dba-tips.blogspot.com/2014/02/oracle-database-administration-scripts.html
# REPORT BUGS to: mahmmoudadel@hotmail.com
