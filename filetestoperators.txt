-e
	file exsits
-f 
	regular file
-s
	not zero size
-d
	directory
-b
	block device
-c
	character device
-p
	file is pipe
-h
	symbolic link
-S
	file is a socket
-t
	associated with terminal device
-r
	has read permission
-w
	has write permission
-x
	has execute permission
-g
	set-group-id flag set on file
-u
	set-user-id flag set on file
-k
	sticky bit set
-O
	owner of file
-G
	group-id of file same as yours
-N
	file modified since it was last read
f1 -nt f2
	file f1 is newer than f2
f1 -ot f2
	file is older than f2
f1 -ef f2
	file f1 and f2 are hard links to the same file
!
	not
