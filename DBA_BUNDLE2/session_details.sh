# #################################################
# Script to get session information	
#					#   #     #
# Author:	Mahmmoud ADEL	      # # # #   ###
# Created:	03-02-11	    #   #   # #   # 
# Modified:	31-12-13	     
#		Customized the script to run on
#		various environments.
#               26-04-14	Added Wait Info
#		29-10-14	AUTO understand
#				user inputs.
#
#
# #################################################

# ###########
# Description:
# ###########
echo
echo "================================================================"
echo "This script Gets SESSION Information on the current instance ..."
echo "================================================================"
echo
sleep 1

# ###########################
# Listing Available Databases:
# ###########################

# Count Instance Numbers:
INS_COUNT=$( ps -ef|grep pmon|grep -v grep|grep -v ASM|wc -l )

# Exit if No DBs are running:
if [ $INS_COUNT -eq 0 ]
 then
   echo No Database Running !
   exit
fi

# If there is ONLY one DB set it as default without prompt for selection:
if [ $INS_COUNT -eq 1 ]
 then
   export ORACLE_SID=$( ps -ef|grep pmon|grep -v grep|grep -v ASM|awk '{print $NF}'|sed -e 's/ora_pmon_//g'|grep -v sed|grep -v "s///g" )

# If there is more than one DB ASK the user to select:
elif [ $INS_COUNT -gt 1 ]
 then
    echo
    echo "Select the ORACLE_SID:[Enter the number]"
    echo ---------------------
    select DB_ID in $( ps -ef|grep pmon|grep -v grep|grep -v ASM|awk '{print $NF}'|sed -e 's/ora_pmon_//g'|grep -v sed|grep -v "s///g" )
     do
        if [ -z "${REPLY##[0-9]*}" ]
         then
          export ORACLE_SID=$DB_ID
          echo Selected Instance:
          echo $DB_ID
          break
         else
          export ORACLE_SID=${REPLY}
          break
        fi
     done

fi
# Exit if the user selected a Non Listed Number:
        if [ -z "${ORACLE_SID}" ]
         then
          echo "You've Entered An INVALID ORACLE_SID"
          exit
        fi

# #########################
# Getting ORACLE_HOME
# #########################
  ORA_USER=`ps -ef|grep ${ORACLE_SID}|grep pmon|grep -v grep|grep -v ASM|awk '{print $1}'|tail -1`
  USR_ORA_HOME=`grep ${ORA_USER} /etc/passwd| cut -f6 -d ':'|tail -1`

## If OS is Linux:
if [ -f /etc/oratab ]
  then
  ORATAB=/etc/oratab
  ORACLE_HOME=`grep -v '^\#' $ORATAB | grep -v '^$'| grep -i "^${ORACLE_SID}:" | perl -lpe'$_ = reverse' | cut -f3 | perl -lpe'$_ = reverse' |cut -f2 -d':'`
  export ORACLE_HOME

## If OS is Solaris:
elif [ -f /var/opt/oracle/oratab ]
  then
  ORATAB=/var/opt/oracle/oratab
  ORACLE_HOME=`grep -v '^\#' $ORATAB | grep -v '^$'| grep -i "^${ORACLE_SID}:" | perl -lpe'$_ = reverse' | cut -f3 | perl -lpe'$_ = reverse' |cut -f2 -d':'`
  export ORACLE_HOME
fi

## If oratab is not exist, or ORACLE_SID not added to oratab, find ORACLE_HOME in user's profile:
if [ -z "${ORACLE_HOME}" ]
 then
  ORACLE_HOME=`grep -h 'ORACLE_HOME=\/' $USR_ORA_HOME/.bash* $USR_ORA_HOME/.*profile | perl -lpe'$_ = reverse' |cut -f1 -d'=' | perl -lpe'$_ = reverse'|tail -1`
  export ORACLE_HOME
fi

# ########################################
# Exit if the user is not the Oracle Owner:
# ########################################
CURR_USER=`whoami`
        if [ ${ORA_USER} != ${CURR_USER} ]; then
          echo ""
          echo "You're Running This Sctipt with User: \"${CURR_USER}\" !!!"
          echo "Please Run This Script With The Right OS User: \"${ORA_USER}\""
          echo "Script Terminated!"
          exit
        fi

# ###############################
# SQLPLUS: Getting Session Info:
# ###############################
echo
echo "Enter the USERNAME or SESSION SID: [Blank value means list all sessions on the current instance]"
echo "=================================="
while read ANS
 do
                 case $ANS in
		 # case the input is non-numeric value:
		 *[!0-9]*) echo
			if [ -z "${ANS}" ]
	 		then

${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' << EOF
set feedback off linesize 180 pages 1000
col module for a27
col event for a29
col "USER | SID,SERIAL# | UNIX_PID" for a40
col "STATUS|WAIT_STATE|TIME_WAITED" for a30
select s.USERNAME||' | '||s.sid||','||s.serial#||' | '||p.spid "USER | SID,SERIAL# | UNIX_PID",
substr(s.MODULE,1,27)"MODULE",
substr(s.status||'|'||w.state||'|'||w.seconds_in_wait||'sec',1,30) "STATUS|WAIT_STATE|TIME_WAITED",
substr(w.event,1,29)"EVENT",s.PREV_SQL_ID,s.SQL_ID CURR_SQL_ID
from v\$session s,v\$process p, v\$session_wait w
where s.USERNAME like upper ('%$ANS%')
and p.addr = s.paddr
and s.sid=w.sid
order by s.USERNAME||' | '||s.sid||','||s.serial#||' | '||p.spid,MODULE;
EOF
			else

${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' << EOF
set feedback off linesize 180 pages 1000
col "UNIX PID" for a8
col username for a16
col module for a27
col event for a30
col WAIT_STATE for a25
col "SID|SERIAL#" for a11
col "Previous SQL" for a140
col "Current SQL" for a140
col "USERNAME | SID,SERIAL#" for a35
Prompt Previous SQL Statement:
prompt -----------------------

select p.spid "UNIX PID",s.USERNAME||' | '||s.sid||','||s.serial# "USERNAME | SID,SERIAL#",substr(s.MODULE,1,27)"MODULE",substr(w.event,1,30)"EVENT",w.state||'|'||w.seconds_in_wait||'sec' WAIT_STATE,s.PREV_SQL_ID,q.SQL_FULLTEXT "Previous SQL"
from v\$session s,v\$process p,v\$sql q, v\$session_wait w
where s.USERNAME like upper ('%$ANS%')
and p.addr = s.paddr
and s.sid=w.sid
and q.child_number=0
and q.sql_id=s.PREV_SQL_ID;

prompt
Prompt Current Running SQL Statement:
prompt ------------------------------

select p.spid "UNIX PID",s.USERNAME||' | '||s.sid||','||s.serial# "USERNAME | SID,SERIAL#",substr(s.MODULE,1,27)"MODULE",substr(w.event,1,30)"EVENT",w.state||'|'||w.seconds_in_wait||'sec' WAIT_STATE,s.SQL_ID CURR_SQL_ID,q.SQL_FULLTEXT "Current SQL"
from v\$process p,v\$session s ,v\$sql q, v\$session_wait w
where s.USERNAME like upper ('%$ANS%')
and p.addr = s.paddr 
and s.sid=w.sid
and q.child_number=0
and q.sql_id=s.sql_id;
EOF
			fi
                        echo; exit ;;
			*) echo
                        if [ -z "${ANS}" ]
                        then
${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' << EOF
set feedback off linesize 180 pages 1000
col "UNIX PID" for a8
col module for a30
col event for a27
col "USERNAME | SID,SERIAL#" for a35
col WAIT_STATE for a25
PROMPT ALL Sessions on the CURRENT Node:
PROMPT
select p.spid "UNIX PID",s.USERNAME||' | '||s.sid||','||s.serial# "USERNAME | SID,SERIAL#",substr(s.MODULE,1,27)"MODULE",w.state||'|'||w.seconds_in_wait||'sec' WAIT_STATE,substr(w.event,1,30)"EVENT",s.PREV_SQL_ID,s.SQL_ID CURR_SQL_ID
from v\$session s,v\$process p, v\$session_wait w
where s.username is not null
and p.addr = s.paddr
and s.sid=w.sid
order by s.USERNAME||' | '||s.sid||','||s.serial#,MODULE;
EOF

                        else

${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' << EOF
set feedback off linesize 180 pages 1000
col "UNIX PID" for a8
col "USERNAME | SID,SERIAL#" for a35
col module for a30
col event for a27
col WAIT_STATE for a25

select p.spid "UNIX PID",s.USERNAME||' | '||s.sid||','||s.serial# "USERNAME | SID,SERIAL#",s.MODULE,w.event,w.state||'|'||w.seconds_in_wait||'sec' WAIT_STATE,s.PREV_SQL_ID "PREV_SQL_ID",s.sql_id "CURR_SQL_ID"
from v\$session s,v\$process p, v\$session_wait w,v\$sql q
where s.SID ='$ANS'
and p.addr = s.paddr
and s.sid=w.sid
and q.child_number=0
and q.sql_id=s.PREV_SQL_ID;

prompt
col "Previous SQL" for a140
select q.SQL_ID,q.SQL_FULLTEXT "Previous SQL"
from v\$process p,v\$session s ,v\$sql q
where s.SID ='$ANS'
and p.addr = s.paddr
and q.sql_id=s.PREV_SQL_ID;

prompt
col "Current SQL" for a140
select q.SQL_ID,q.SQL_FULLTEXT "Current SQL"
from v\$process p,v\$session s ,v\$sql q
where s.SID ='$ANS'
and p.addr = s.paddr
and q.sql_id=s.sql_id;
EOF
			fi
		echo; exit ;;
		esac
 done

# #############
# END OF SCRIPT
# #############
# REPORT BUGS to: <mahmmoudadel@hotmail.com>.
# DISCLAIMER: THIS SCRIPT IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT WITHOUT ANY WARRANTY. IT IS PROVIDED "AS IS".
# DOWNLOAD THE LATEST VERSION OF DATABASE ADMINISTRATION BUNDLE FROM: http://dba-tips.blogspot.com/2014/02/oracle-database-administration-scripts.html
