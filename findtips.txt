You can toggle between the last two current directories using cd �C

The following command looks for all the files under /etc directory with mail in the filename.
# find /etc -name "*mail*"

The following command will list all the files in the system greater than 100MB.
# find / -type f -size +100M

The following command will list all the files that were modified more than 60 days ago under the current directory.
# find . -mtime +60

The following command will list all the files that were modified in the last two days under the current directory.
# find . �Cmtime -2

Please be careful while executing the following command as you don��t want to delete the files by mistake. The best practice is to execute the same command with ls �Cl to make sure you know which files will get deleted when you execute the command with rm.
# find / -type f -name *.tar.gz -size +100M -exec ls -l {} \;
# find / -type f -name *.tar.gz -size +100M -exec rm -f {} \;

The following command finds all the files not modified in the last 60 days under /home/jsmith directory and creates an archive files under /tmp in the format of ddmmyyyy_archive.tar.
# find /home/jsmith -type f -mtime +60 | xargs tar -cvf /tmp/`date '+%d%m%Y'_archive.tar`

On a side note, you can perform lot of file related activities (including finding files) using midnight commander GUI, a powerful text based file manager for Unix.