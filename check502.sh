#!/bin/bash
date_ymd=`date +%F`
time_hms=`date +%H:%M":00"`
time_hms2=`date -d '-10 min' +%H:%M":00"`
beg_time=$date_ymd'T'$time_hms2'+08:00'
end_time=$date_ymd'T'$time_hms'+08:00'

cat  /mall/web/nginx/logs/8224.access.log |awk '$1 >= '\"$beg_time\"' && $1 < '\"$end_time\"' {print $0}' > tmp.txt

num=`cat tmp.txt | grep 'HTTP/1.1" 502' | wc -l`

if [[ $num -gt 100 ]];then
	echo printf '"'36 nginx 在十分钟内出现 502 超过 100 次! ${num} 次! `date +%F-%T`'"'> /backup/message_alarm
/36_nginx_502/conf/msg.txt
	ssh 172.18.10.48 "/backup/message_alarm/36_nginx_502/message.sh"
	echo `date +%H:%M:%S` >> file/`date +%Y%m%d`.log
	echo "error!" $num >> file/`date +%Y%m%d`.log
else
	echo `date +%H:%M:%S` >> file/`date +%Y%m%d`.log
	echo "ok!" $num  >> file/`date +%Y%m%d`.log
fi
