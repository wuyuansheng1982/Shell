-eq
	is equal to
-ne
	is not equal to
-gt
	is greater than
-ge
	is greater than or equal to
-lt
	is less than
-le
	is less than or equal to
<
	is less than (within double parentheses)
<=
	is less than or equal to (within double parentheses)
>
	is greater than (withink double parentheses)
>=
	is greater than or equal to (within double parentheses)
string
=
	is equal to
==
	is equal to
!=
	is not equal to
<
	is less than, in ASCII alphabetical order
-z
	string is null
-n
	string is not null
